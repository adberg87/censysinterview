from censys.search import CensysHosts
import re

def search(queryString):
	h = CensysHosts()
	query = h.search(
		queryString, 
		per_page=50,
		virtual_hosts="INCLUDE"
	)
	return query.view_all()

def hostServices(host):
	services = result[host]['services']
	return services
	
def getService(host, serviceType):
    services = hostService(host)
	for service in services:
		if service['_decoded'] == serviceType:
			return service

def iterateTags(httpService):
	if 'html_tags' in service['http']['response'].keys():
		tags = service['http']['response']['html_tags']
	return tags

host_list = []
version_count = {}
ajs-version = 'ajs-version-number'
result = search('services.service_name: HTTP AND services.http.response.body:`*{0]*`'.format(ajs-version))

for host in result.keys():
	service_type = getService(host, 'HTTP')
	tags = iterageTags(service_type)
	for tag in tags:
		if ajs-version in tag:
			version = re.findall("[0-9].[0-9]{1,2}.[0-9]{1,2}",tag)
			host_list.append({'ip':host, 'port':service['port'], 'version':version[0]})
			if version[0] in version_count:
				version_count[version[0]] += 1
			else:
				version_count[version[0]] = 1

print(host_list)
print(version_count)
