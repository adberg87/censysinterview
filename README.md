# CensysInterview

## Research

In analysis of the provided host, I found that it was Confluence server, being served by Nginx. The version of Confluence was "7.13.2". The headers that are captured by Censys provide the webserver ("nginx"), while the meta fields in the HTTP response body reveal the Confluence version. It is noted that this is expected behavior and would take alteration of the Confluence code to hide this.

## Development

I kept the python relatively simple, and tailored it for queries of Confluence versions. The code provides a simple count of the versions in the sample query, plus a breakdown of what hosts and their version. 

## Analysis
`
{'7.19.9': 4, '7.13.7': 9, '7.14.3': 12, '7.20.1': 2, '8.4.0': 1, '7.13.0': 1, '7.19.6': 2, '7.13.1': 1, '7.13.8': 1, '7.19.3': 2, '6.13.23': 4, '6.15.4': 1, '7.17.2': 2, '7.18.1': 4, '9.4.8': 1, '7.12.5': 1, '9.2.0': 2, '7.4.11': 1}'
